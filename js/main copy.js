/* Odontograma v1.0
	Propietario: Luis Lopez
	Desarrollado por Oscar Jimmy
	Librerias utilizadas: konvajs v7.0.3, jspdf v1.5.3, bootstrap-3,color-picker v2.1.6
	
	*** 1.-Variables
	*** 2.- Funciones
	*** 3.- Eventos mouse, teclado, movil
*/

//variables
var stageWidth = 900;
var stageHeight = 570;
var backG;
var isPaint;
var deleteObj = false;
var addIcon;
var activar;
var textNode;
var mode = "brush";
var lastLine;
var stage = new Konva.Stage({
	container: "container",
	width: stageWidth,
	height: stageHeight,
});

//creando capa
var layer = new Konva.Layer();
stage.add(layer);

//fondo
var imageObj = new Image();
imageObj.onload = function () {
	backG = new Konva.Image({
		x: 0,
		y: 0,
		image: imageObj,
		width: stage.width(),
		height: stage.height(),
		name: "imagenFondo",
	});
	stage.draw();
	layer.add(backG);
	layer.batchDraw();
};

//imagen fondo
imageObj.src = "img/dientes-odontograma.png";
var tr = new Konva.Transformer({ name: "rectTransformer" });
layer.add(tr);
layer.draw();

/*2.- funciones *****/
// responsive
function fitStageIntoParentContainer() {
	var container = document.querySelector("#stage-parent");
	var containerWidth = container.offsetWidth;
	var containerHeight = container.offsetHeight;
	var scale = containerWidth / stageWidth;
	stage.width(stageWidth * scale);
	stage.height(stageHeight * scale);
	imageObj.width = stageWidth * scale;
	imageObj.height = stageHeight * scale;
	var shapes = stage.find(".imagenFondo");
	shapes.each(function (shape) {
		shape.attrs.width = containerWidth;
		shape.attrs.height = containerHeight;
		shape.draw();
	});
	stage.draw();
	layer.batchDraw();
}

//color
function onChange(r, g, b, a) {
	var bb2 = Array.from(document.getElementsByClassName("button-color-draw"));
	bb2.forEach((button2) => {
		button2.classList.remove("button-selected-draw");
	});
	this.source.value = this.color(r, g, b, a);
	document.querySelector("#btnbc").style.backgroundColor = this.color(
		r,
		g,
		b,
		a
	);
	deleteObj = false;
	document.querySelector("#colorPincel").value = this.color(r, g, b, a);
}

//generar pdf
document.getElementById("generarPdf").addEventListener("click", function () {
	var pdf = new jsPDF("l", "px");
	pdf.addImage(
		stage.toDataURL({ pixelRatio: 1.5 }),
		"PNG",
		50,
		50,
		pdf.internal.pageSize.getWidth() - 100,
		pdf.internal.pageSize.getHeight() - 100
	);
	var datePdf = new Date();
	pdf.setTextColor(0, 0, 0);
	pdf.setFontSize(22);
	pdf.setFontType("bold");
	pdf.text(pdf.internal.pageSize.getWidth() / 2, 30, "ODONTOGRAMA", "center");

	pdf.setTextColor(0, 0, 0);
	pdf.setFontSize(12);
	pdf.setFontType("normal");
	pdf.text(50, 30, "Odontologo:");
	pdf.text(50, 40, "Dr: Nombre Doctor");

	pdf.setTextColor(0, 0, 0);
	pdf.setFontType("normal");
	pdf.setFontSize(12);
	pdf.text(pdf.internal.pageSize.getWidth() - 150, 30, "Paciente:");
	pdf.text(pdf.internal.pageSize.getWidth() - 150, 40, "Nombre Paciente");

	pdf.setTextColor(0, 0, 0);
	pdf.setFontType("normal");
	pdf.setFontSize(10);
	pdf.text(
		pdf.internal.pageSize.getWidth() - 200,
		pdf.internal.pageSize.getHeight() - 20,
		`Fecha Impresion: ${datePdf.getDate()}/0${
			datePdf.getMonth() + 1
		}/${datePdf.getFullYear()} Hora:${datePdf.getHours()}:${datePdf.getMinutes()}:${datePdf.getSeconds()}`
	);
	var nameFile = `Odontograma-${datePdf.getDate()}0${
		datePdf.getMonth() + 1
	}${datePdf.getFullYear()}${datePdf.getHours()}${datePdf.getMinutes()}${datePdf.getSeconds()}.pdf`;
	//pdf.save(`Odontograma-${datePdf.getDate()}0${datePdf.getMonth() + 1}${datePdf.getFullYear()}${datePdf.getHours()}${datePdf.getMinutes()}${datePdf.getSeconds()}.pdf`);
	pdf.setProperties({ title: nameFile});
	//string pdf
	var pdfOutput=pdf.output("bloburl", [nameFile]);
	console.log(pdfOutput)
	//mostrar en nueva pagina
	window.open(pdfOutput);
});

// 3.- eventos mouse canvas
stage.on("mousedown touchstart", function (e) {
  
	if (activar) {
		isPaint = true;
	}

	var pos = stage.getPointerPosition();
	lastLine = new Konva.Line({
		stroke: document.querySelector("#colorPincel").value,
		strokeWidth: document.querySelector("#sizePincel").value,
		name: "linea",
    	//draggable:true,
		globalCompositeOperation:
			mode === "brush" ? "source-over" : "destination-out",
		points: [pos.x, pos.y],
	});
	layer.add(lastLine);
});

//Clear All
document.getElementById("clearAll").addEventListener("click", function () {
	var lines = stage.find("Line");
	var img = stage.find(".iconos");
	var textF = stage.find(".textCustom");
	var group = new Konva.Group();
	group.add(lines, img, textF);
	group.destroy();
	stage.draw();
	layer.draw();
});

//agregar texto
document.getElementById("addText").addEventListener("click", function () {
	deleteObj = false;
	activar = false;
	addIcon = false;
	var colorText = document.querySelector("#colorPincel").value;
	textNode = new Konva.Text({
		text: "Texto",
		x: stage.width() / 2,
		y: stage.height() / 2,
		name: "textCustom",
		align: "center",
		fontSize: 30,
		fill: colorText,
		draggable: true,
	});

	layer.add(textNode);
	layer.draw();
	textNode.on("dblclick dbltap", () => {
		var textPosition = textNode.getAbsolutePosition();
		var stageBox = stage.container().getBoundingClientRect();
		var areaPosition = {
			x: stageBox.left + textPosition.x,
			y: stageBox.top + textPosition.y,
		};
		var textarea = document.createElement("textarea");
		document.body.appendChild(textarea);
		textarea.value = textNode.text();
		textarea.style.position = "absolute";
		textarea.style.top = areaPosition.y + "px";
		textarea.style.left = areaPosition.x + "px";
		textarea.style.width = textNode.width();
		textarea.focus();
		textarea.addEventListener("keydown", function (e) {
			// hide on enter
			if (e.keyCode === 13 || e.key === 13) {
				textNode.text(textarea.value);
				layer.draw();
				document.body.removeChild(textarea);
			}
		});
	});
});

stage.on("mouseup touchend", function () {
	isPaint = false;
  
});


stage.on("mousemove touchmove", function () {
	if (!isPaint) {
		return;
	}
	const pos = stage.getPointerPosition();
	var newPoints = lastLine.points().concat([pos.x, pos.y]);
	lastLine.points(newPoints);
	layer.batchDraw();
});

//drop
var itemURL = "";
document
	.getElementById("drag-items")
	.addEventListener("dragstart", function (e) {
    deleteObj = false;
		activar = false;
		itemURL = e.target.src;
	});

  document
	.getElementById("drag-items")
	.addEventListener("click", function (e) {
    deleteObj = false;
		activar = false;
		itemURL = e.target.src;
	});
  document
	.getElementById("drag-items")
	.addEventListener("touchstart", function (e) {
		activar = false;
		itemURL = e.target.src;
		if(addIcon){
			stage.setPointersPositions();
			Konva.Image.fromURL(itemURL, function (image) {
			layer.add(image);
			nombre = Math.floor(Math.random() * 10).toString();
			image.name("iconos");
			image.draggable(true);
			image.position({
			  x: stage.getPointerPosition().x - 15,
			  y: stage.getPointerPosition().y - 15,
			  });
			  layer.draw();
			});
		  }
	});

var con = stage.container();
con.tabIndex = 1;

//con.focus();
con.addEventListener("keydown", function (e) {
	if (stage.clickEndShape.attrs.name === "imagenFondo") {
		return;
	}
	if (e.keyCode === 27 || e.key === 27) {
		stage.setPointersPositions(e);
		stage.clickEndShape.destroy();
		layer.find(".rectTransformer").destroy();
	} else {
		return;
	}
	e.preventDefault();
	layer.draw();
});

con.addEventListener("dragover", function (e) {
	e.preventDefault(); // !important
});
con.addEventListener("touchmove", function (e) {
	e.preventDefault(); // !important
});

con.addEventListener("drop", function (e) {
	e.preventDefault();
	stage.setPointersPositions(e);
	Konva.Image.fromURL(itemURL, function (image) {
		layer.add(image);
		nombre = Math.floor(Math.random() * 10).toString();
		image.name("iconos");
		image.draggable(true);
		image.position({
			x: stage.getPointerPosition().x - 15,
			y: stage.getPointerPosition().y - 15,
		});
		layer.draw();
	});
});

stage.addEventListener("touchend", function (e) {
	e.preventDefault();
	if (addIcon) {
		stage.setPointersPositions(e);
		Konva.Image.fromURL(itemURL, function (image) {
			layer.add(image);
			nombre = Math.floor(Math.random() * 10).toString();
			image.name("iconos");
			image.position({
				x: stage.getPointerPosition().x - 15,
				y: stage.getPointerPosition().y - 15,
			});
			image.draggable(true);
			layer.draw();
		});
	}
});

stage.on("click tap", function (e) {
	layer.find(".rectTransformer").destroy();
	layer.draw();


	if (deleteObj) {
		if (
			e.target.attrs.name === "imagenFondo" ||
			e.target.attrs.name === undefined
		) {
			return;
		} else {
			e.target.destroy();
			layer.draw();
			return;
		}
	}

	layer.find(".rectTransformer").destroy();
	if (!e.target.hasName("iconos") && !e.target.hasName("textCustom")) {
		return;
	}

	var tr = new Konva.Transformer({ name: "rectTransformer" });
	layer.add(tr);
	tr.nodes([e.target]);
	layer.draw();
});

//document Resize
window.addEventListener("resize", fitStageIntoParentContainer);

//document ready
document.addEventListener("DOMContentLoaded", function (event) {
	
	fitStageIntoParentContainer();
	//color
	var picker = new CP(document.querySelector("#btnbc"));
	picker.on("change", onChange);

	//cambiar colores primario
	var bb = Array.from(document.getElementsByClassName("button-color"));
	bb.forEach((button) => {
		button.addEventListener("click", () => {
			deleteObj = false;
			addIcon = false;
			bb.forEach((button) => {
				button.classList.remove("button-selected");
			});
			button.classList.add("button-selected");
			document.querySelector("#colorPincel").value =
				button.style.backgroundColor;
		});
	});

	//Seleccion pincel y simbolos
	var btnAction = document.querySelectorAll(".button-color-draw");
	if (btnAction) {
		btnAction.forEach(function (el, key) {
			el.addEventListener("click", function () {
				el.classList.toggle("active");
				if (el.classList.contains("active")) {
					if (el.id === "addpencil") {
						activar = true;
						deleteObj = false;
						addIcon = false;
					} else if (el.id === "erasepencil") {
						activar = false;
						deleteObj = true;
						addIcon = false;
					} else {
						activar = false;
						deleteObj = false;
						addIcon = true;
					}
				} else {
					activar = false;
					deleteObj = false;
					addIcon = false;
				}
				btnAction.forEach(function (ell, els) {
					if (key !== els) {
						ell.classList.remove("active");
					}
				});
			});
		});
	}

		//Seleccion tamaño pincel
		var btnSize = document.querySelectorAll(".button-pencils");
		if (btnSize) {
			btnSize.forEach(function (el, key) {
				el.addEventListener("click", function () {
					el.classList.toggle("active-pincel");
					if (el.classList.contains("active-pincel")) {
						console.log(el.id);

						if (el.id === "s3") {
							document.querySelector("#sizePincel").value = 3;
						} else if (el.id === "s6") {
							document.querySelector("#sizePincel").value = 6;
						} else if (el.id === "s9") {
							document.querySelector("#sizePincel").value = 9;
						}else if (el.id === "s12") {
							document.querySelector("#sizePincel").value = 12;
						}else {
							
						}
					} else {
						
					}
					btnSize.forEach(function (ell, els) {
						if (key !== els) {
							ell.classList.remove("active-pincel");
						}
					});
				});
			});
		}
});
